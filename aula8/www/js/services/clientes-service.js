/*** factory clientes ***/
(function() {
	'use strict';
	angular.module('treinamentoSilex')
    .factory('ClientesService', function ClientesService($q, $http, DREAMFACTORY_CONFIG) {
        var factory = {};
        
        factory.getClientes = function() {
        	var deferred = $q.defer();
        	
        	$http.get(DREAMFACTORY_CONFIG.BASE_URL + DREAMFACTORY_CONFIG.APP_NAME + '/_table/clientes')
        	.then(
        		function(returnApi) {
        			console.log(returnApi.data.resource);
        			deferred.resolve(returnApi.data.resource);
        		},
        		function(returnApi) {
        			console.log(returnApi);
        			deferred.reject(false);
        		}
        	);
        	
        	return deferred.promise;
        };
        
        // metodo para recuperar dados de um cliente atraves do id
        factory.getCliente = function(clienteId) {
        	var deferred = $q.defer();
        	
        	$http.get(DREAMFACTORY_CONFIG.BASE_URL + DREAMFACTORY_CONFIG.APP_NAME + '/_table/clientes?filter='+encodeURI("id="+clienteId))
        	.then(
        		function(returnApi) {
        			console.log(returnApi.data.resource[0]);
        			deferred.resolve(returnApi.data.resource[0]);
        		},
        		function(returnApi) {
        			console.log(returnApi);
        			deferred.reject(false);
        		}
        	);
        	
        	return deferred.promise;
        };
        
        // metodo para editar dados de um cliente
        factory.editCliente = function(cliente) {
        	var deferred = $q.defer();
        	
        	$http({
        		url: DREAMFACTORY_CONFIG.BASE_URL + DREAMFACTORY_CONFIG.APP_NAME + '/_table/clientes/'+cliente.id,
        		method: "PATCH",
        		data: cliente
        	}).then(
        		function(returnApi) {
        			console.log(returnApi);
        			deferred.resolve(true);
        		},
        		function(returnApi) {
        			console.log(returnApi);
        			deferred.reject(false);
        		}
        	);
        	
        	return deferred.promise;
        };
        
        // metodo para adicionar um cliente
        factory.addCliente = function(cliente) {
        	var deferred = $q.defer();
        	
        	// encapsula o cliente no objeto resource
        	var resourceObj = {
    		  resource: [ cliente ]
    		};
        	
        	$http({
        		url: DREAMFACTORY_CONFIG.BASE_URL + DREAMFACTORY_CONFIG.APP_NAME + '/_table/clientes',
        		method: "POST",
        		data: resourceObj
        	}).then(
        		function(returnApi) {
        			console.log(returnApi.data.resource[0].id);
        			deferred.resolve(returnApi.data.resource[0].id);
        		},
        		function(returnApi) {
        			console.log(returnApi);
        			deferred.reject(false);
        		}
        	);
        	
        	return deferred.promise;
        };
        
        return factory;
    });
})();