(function() {
    'use strict';
    angular.module('treinamentoSilex')
    .controller('LoginController', function($scope, $rootScope, $state, LoginService) {
        $scope.doLogin = function() {
        	$rootScope.showLoading = true;
        	
        	if ($scope.loginForm.$valid) {
                LoginService.doLogin($scope.login).then(function(loggedUser) {
                	$rootScope.showLoading = false;
                	$state.go('home');
                },function(error) {
                	$rootScope.showLoading = false;
                	$rootScope.openModalError("Usuário ou senha inválidos!", function(error) {});
                });
            } else {
            	$rootScope.showLoading = false;
            	$rootScope.openModalError("Preencha os campos corretamente");
            }
        }
	})
})();