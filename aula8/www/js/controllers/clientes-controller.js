(function() {
    'use strict';
    angular.module('treinamentoSilex')
    .controller('ClientesController', function($scope, $rootScope, $state, $filter, ClientesService) {
    	var getCliente = function() {
	    	ClientesService.getCliente($state.params.clienteId).then(
	    		function(data) {
	    			data.senha = "";
	    			$scope.cliente = data;
	    			$scope.cliente.data_nascimento = new Date($scope.cliente.data_nascimento);
	    			$scope.idadeCalculada = $filter('calcAge')($scope.cliente.data_nascimento);
	    		},
	    		function(error) {
	    			 alert("Erro ao recuperar dados do cliente");
	    		}
	    	);
    	};
    	
    	if ($state.current.name == "editarClientes") {
    		$scope.titulo = "Edição de cliente";
			getCliente();
    	} else {
			$scope.titulo = "Inserção de cliente";
		}
    	
    	$scope.saveCliente = function() {
    		// recupera os dados do form
    		var clienteObj = $scope.cliente;
    		
    		// valida senha preenchida e apaga se ela nao existir
    		if (!angular.isDefined($scope.cliente.senha) || $scope.cliente.senha == "") {
    			delete clienteObj.senha;
    		}
    		
    		if ($state.current.name == "editarClientes") {
	    		// chama metodo para editar cliente
	    		ClientesService.editCliente(clienteObj).then(
	    			function(data) {
	    				alert("Cliente salvo com sucesso");
	    			},
	    			function(error){
	    				alert("Erro ao salvar cliente");
	    			}
	    		);
    		} else if($state.current.name == "addClientes") {
    			// chama metodo para salvar cliente
	    		ClientesService.addCliente(clienteObj).then(
	    			function(data) {
	    				alert("Cliente salvo com sucesso");
	    				$state.go("editarClientes", { clienteId: data });
	    			},
	    			function(error){
	    				alert("Erro ao salvar cliente");
	    			}
	    		);
    		} else {
    			alert("Rota incorreta");
    		}
    	};
    })
})();