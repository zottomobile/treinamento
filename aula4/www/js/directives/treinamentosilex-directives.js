/*** directives for nomeDoProjeto ***/
(function() {
	'use strict';
	angular.module('treinamentoSilex')
	// directive for loading-spinner
	.directive('loadingSpinner', function(){
		return {
			restrict: "E",
			templateUrl: 'partials/loading-spinner.html',
			scope: {
				textLoading: '='
			}
		}
	})
	// directive for side-menu
	.directive('sideMenu', function($location){
		return {
			restrict: "E",
			templateUrl: "partials/side-menu.html",
			scope: {
				sideMenuVisible: "="
			},
			link: function(scope, elem, attrs) {
				// get a element slidemenu and bind click event to close menu
				var queryResult = elem[0].querySelector('#slidemenu');
				angular.element(queryResult).bind('click', function() {
					// set variable to hide menu and execute apply because in this context the variable in view does not automatically updated
					scope.sideMenuVisible = false;
					scope.$apply();
				});

				// method invoked in click item menu
				scope.selectMenu = function($event, menuClicked) {
					// stop propagation to avoid call method binded to menu
					$event.stopPropagation();

					// redirect to route according menu clicked
					switch(menuClicked) {
						case "splash":
							$state.go("/splash");
							break;
						default:
							$state.go("/splash");
							break;
					};

					// set variable to hide menu
					scope.sideMenuVisible = false;
				};
			},
			controller: function($scope, $rootScope) {
				// listener to event openSideMenu
				$rootScope.$on('openSideMenu', function(){
					$scope.sideMenuVisible = true;
				});
				// listener to event closeSideMenu
				$rootScope.$on('closeSideMenu', function(){
					$scope.sideMenuVisible = false;
				});
			}
		}
	})
	// directive for header-bar
	.directive('headerBar', function(){
		return {
			restrict: "E",
			templateUrl: "partials/header-bar.html",
			scope: {
				nameMenu: "=",
				showIco: "="
			},
			link: function(scope, element, attrs) {
				// set imageMenu according nameMenu
				switch (scope.nameMenu) {
					case "splash":
						scope.imageMenu = "splash.png";
						scope.titleBar = "Splash";
						break;
					default:
						scope.imageMenu = "splash.png";
						scope.titleBar = "Splash";
						break;
				};
			},
			controller: function($scope, $rootScope) {
				// method do invoke event to open menu
				$scope.openSideMenu = function(){
					$rootScope.$emit('openSideMenu');
				};
			}
		}
	})
	// directive for header-with-back
	.directive('headerWithBack', function($state, $window){
		return {
			restrict: "E",
			templateUrl: 'partials/header-with-back.html',
			controller: function($scope) {
				// method for back button
				$scope.back = function() {
					switch ($state.current.name) {
						case 'register':
							$state.go('splash');
							break;
						default:
							$window.history.back();
							break;
					}
				};
			}
		}
	})
	// directive for header-withoutback
	.directive('headerWithoutBack', function(){
		return {
			restrict: "E",
			templateUrl: 'partials/header-without-back.html'
		}
	});
})();