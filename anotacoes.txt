Instalar o gradle
https://gradle.org/install

npm install -g cordova

Comandos cordova
cordova create pastaDoProjeto br.com.teste Teste

cordova platform add android --save
cordova platform add ios --save
cordova platform rm android

cordova plugin add cordova-plugin-whitelist
cordova plugin rm cordova-plugin-whitelist
cordova plugin add cordova-plugin-facebook4 --save --variable APP_ID="123456789" --variable APP_NAME="myApplication"

cordova prepare

cordova build android




gerar chave para assinatura do aplicativo
keytool -genkey -v -keystore treinamentosilex-key.keystore -alias treinamentosilex -keyalg RSA -keysize 2048 -validity 10000

Senha da keystore: Z0tt0#!7

gerar hash para facebook
keytool -exportcert -alias treinamentosilex -keystore treinamentosilex-key.keystore | openssl sha1 -binary | openssl base64

hash para facebook: aGtqo++xaS4DWILzuYWCDp+YvIk=

assinar apk
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore treinamentosilex-key.keystore platforms/android/build/outputs/apk/android-release-unsigned.apk TreinamentoSilex

reduzir tamanho do apk (comando indicado pelo Google)
zipalign -v 4 platforms/android/build/outputs/apk/android-release-unsigned.apk TreinamentoSilex.apk

------------------------
Acessos do banco de dados

host: 35.165.150.232
user: treinamento
pass: Zot70!Tr

------------------------------------------------
Dreamfactory

api.fidellius.com.br
user: treinamento@zottomobile.com.br
pass: #tr3in4!

user: api@zottomobile.com.br
pass: #tr3in4!

APP Name: treinamento
API Key: 4ed53e43204e760def9c488e0cf267e273b71d580acc737726b4634e2a41f380