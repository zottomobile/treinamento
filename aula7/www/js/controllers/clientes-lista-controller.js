(function() {
    'use strict';
    angular.module('treinamentoSilex')
    .controller('ClientesListaController', function($scope, $rootScope, $state, ClientesService) {
    	$rootScope.showLoading = true;
    	$rootScope.textLoading = "Carregando clientes";
    	
    	ClientesService.getClientes().then(
    		function(data) {
    			$scope.clientes = data;
    			$rootScope.showLoading = false;
    		},
    		function(error) {
    			$rootScope.showLoading = false;
    			alert("Erro ao recuperar clientes");
    		}
    	);
    	
    	// metodo para redirecionar para edicao de cliente
    	$scope.redirectEdit = function(cliente) {
    		var clienteId = cliente.id;
    		// o direcionamento passando parametro por url, precisa do nome da rota
    		// e tambem de um objeto com o nome do parametro que foi definido na rota
    		// Ex: $state.go("nomeDaRota", { nomeDoParametro: valor })
    		$state.go("editarClientes", { clienteId: clienteId });
    	};
    	
    	$scope.redirectAdd = function() {
    		$state.go("addClientes");
    	};
    })
})();