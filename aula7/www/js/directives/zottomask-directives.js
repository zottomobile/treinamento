angular.module('zottoMask-directives', []);
angular.module('zottoMask-directives')
  .directive('moneyMask', ['$log', '$timeout', '$filter', 'deviceDetector', function($log, $timeout, $filter, deviceDetector) {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, element, attrs) {
        var valor = "";
        element.on('keyup', function(e) {
          if (deviceDetector.isMobile()){
            var temp = element.val().replace('R$', '');
            temp = temp.replace(',', '');
            temp = temp.replace('.', '');
            temp = temp.replace('.', '');
            temp = temp.replace('*', '');
            temp = temp.replace('#', '');
            if (temp.length == 0){
              element.val($filter('currency')('0.0'));
            } else if (temp.length == 1){
              element.val($filter('currency')('0.0' + temp));
            } else if (temp.length == 2){
              element.val($filter('currency')('0.' + temp));
            } else if (temp.length == 10){
              temp = temp.substr(0, temp.length - 2);
              element.val($filter('currency')(temp.substr(0, temp.length - 2) + "." + temp.substr(temp.length - 2, temp.length - 1)));
            } else if (temp.length > 2){
              element.val($filter('currency')(temp.substr(0, temp.length - 2) + "." + temp.substr(temp.length - 2, temp.length - 1)));
            }
            scope.$parent.returnValue = element.val();
          } else {
            if ((parseInt(e.key) || parseInt(e.key) == 0) && e.key >= 0 && e.key < 10){
              valor += e.key.toString();
            } else {
              if (e.key == 'Backspace' || e.key == 'Delete'){
                if (valor.length == 0){

                } else if (valor.length == 1){
                  valor = "";
                } else if (valor.length == 2){
                  valor = valor.substr(0, 1);
                } if (valor.length > 2){
                  valor = valor.substr(0, valor.length - 2);
                }
              }
            }
            if (valor.length == 0){
              element.val($filter('currency')('0.0'));
            } else if (valor.length == 1){
              element.val($filter('currency')('0.0' + valor));
            } else if (valor.length == 2){
              element.val($filter('currency')('0.' + valor));
            } else if (valor.length > 2){
              element.val($filter('currency')(valor.substr(0, valor.length - 2) + "." + valor.substr(valor.length - 2, valor.length - 1)));
            }
            scope.$parent.returnValue = element.val();
          }
        });
      }
    }
  }]);
