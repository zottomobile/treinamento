(function() {
    'use strict';
    angular.module('treinamentoSilex')
    .controller('ClientesController', function($scope, $rootScope, $state, ClientesService) {
    	var getCliente = function() {
	    	ClientesService.getCliente($state.params.clienteId).then(
	    		function(data) {
	    			data.senha = "";
	    			$scope.cliente = data;
	    		},
	    		function(error) {
	    			 alert("Erro ao recuperar dados do cliente");
	    		}
	    	);
    	};
    	
    	if ($state.current.name == "editarClientes") {
    		getCliente();
    	};
    	
    	$scope.saveCliente = function() {
    		// recupera os dados do form
    		var clienteObj = $scope.cliente;
    		
    		// valida senha preenchida e apaga se ela nao existir
    		if (!angular.isDefined($scope.cliente.senha) || $scope.cliente.senha == "") {
    			delete clienteObj.senha;
    		}
    		
    		if ($state.current.name == "editarClientes") {
	    		// chama metodo para editar cliente
	    		ClientesService.editCliente(clienteObj).then(
	    			function(data) {
	    				alert("Cliente salvo com sucesso");
	    			},
	    			function(error){
	    				alert("Erro ao salvar cliente");
	    			}
	    		);
    		} else if($state.current.name == "addClientes") {
    			// chama metodo para salvar cliente
	    		ClientesService.addCliente(clienteObj).then(
	    			function(data) {
	    				alert("Cliente salvo com sucesso");
	    				$state.go("editarClientes", { clienteId: data });
	    			},
	    			function(error){
	    				alert("Erro ao salvar cliente");
	    			}
	    		);
    		} else {
    			alert("Rota incorreta");
    		}
    	};
    })
})();