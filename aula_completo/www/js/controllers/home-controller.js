(function() {
    'use strict';
    angular.module('treinamentoSilex')
    .controller('HomeController', function($scope, $rootScope, $localForage, $state) {
        $localForage.getItem('userLogged').then(function(data) {
			$scope.nomeUsuario = data.nome;
		});
        
        $scope.logout = function() {
        	$localForage.setItem('userLogged', {}).then(function(data){
        		$scope.nomeUsuario = "";
        		$state.go('login');
        	});
        };
        
        $scope.redirectClientes = function() {
        	$state.go('listaClientes');
        };
	})
})();