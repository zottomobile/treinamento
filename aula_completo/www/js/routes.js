/** routes file **/
(function() {
	'use strict';
	angular.module('treinamentoSilex')
	.config(function($stateProvider, $locationProvider, $urlRouterProvider){
        $urlRouterProvider.otherwise('/');
        $stateProvider
		.state('default', {
            url: '/',
            templateUrl: "partials/splash.html",
            controller: 'SplashController'
        }).state('splash', {
			url: '/splash',
			templateUrl: "partials/splash.html",
			controller: 'SplashController'
		}).state('login', {
			url: '/login',
			templateUrl: "partials/login.html",
			controller: 'LoginController'
		}).state('home', {
			url: '/home',
			templateUrl: "partials/home.html",
			controller: 'HomeController'
		}).state('listaClientes', {
			url: '/clientes-lista',
			templateUrl: "partials/clientes-lista.html",
			controller: 'ClientesListaController'
		}).state('editarClientes', {
			url: '/clientes-edit/:clienteId',
			templateUrl: "partials/clientes-form.html",
			controller: 'ClientesController',
			params: {
				cliente: null
			}
		}).state('addClientes', {
			url: '/clientes-add/:clienteId',
			templateUrl: "partials/clientes-form.html",
			controller: 'ClientesController'
		});
	});
})();